package com.preEmynence.dao;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}
